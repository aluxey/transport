package transport

// Network manager interface
type Manager interface {
	Send(mess Message, recipientAddr string) error
	RegisterObserver(messType MessageType, ch chan<- Message) error
}

type MessageType string

type Message struct {
	Type    MessageType
	Payload []byte
}
